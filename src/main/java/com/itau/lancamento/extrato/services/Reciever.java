package com.itau.lancamento.extrato.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.itau.lancamento.extrato.models.Extrato;

import com.itau.lancamento.extrato.repositories.ExtratoRepository;

@Component
public class Reciever implements Runnable{
	@Autowired
	static ExtratoRepository extratoRepository;

	@JmsListener(destination = "produtos.queue.lancamentofinanceiro")

	public static void recieveMessage(Extrato mensagemProduto) {
		while (true) {

			Extrato extrato = mensagemProduto;

			extratoRepository.save(extrato);

			try {
				System.out.println("Aguardando mensagens de lancamento");

			} catch (Exception e) {
				System.out.println("Erro: " + e.getMessage());
			}

		}
	}

	public void run() {
		System.out.println("Lancamento recebido com sucesso");
		
	}

}
