package com.itau.lancamento.extrato.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.lancamento.extrato.models.Extrato;


public interface  ExtratoRepository extends CrudRepository<Extrato, Long> {

}
