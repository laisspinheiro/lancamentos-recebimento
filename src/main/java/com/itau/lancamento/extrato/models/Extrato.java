package com.itau.lancamento.extrato.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sun.istack.NotNull;

@Entity
public class Extrato {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	private String cpf;
	
	private String sigla;
	
	private String tipoOperacao;
	
	private String descricao;
	
	private String observacao;
	
	@NotNull
	private double valor;
	
	@NotNull
	private LocalDate data;
	
	@NotNull
	private String acao;
	
	@NotNull
	private String contrato;

}
